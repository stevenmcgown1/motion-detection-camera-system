FROM python:3.8

WORKDIR /app

RUN pip install datetime opencv-python==4.1.2.30
RUN apt-get update ##[edited]
RUN apt-get install ffmpeg libsm6 libxext6  -y

COPY . /app

CMD ["python3", "./app/main.py"]

#sudo docker run --rm -ti --net=host --ipc=host -e DISPLAY=$DISPLAY 
#-v /tmp/.X11-unix:/tmp/.X11-unix --env="QT_X11_NO_MITSHM=1" --device=/dev/video0 <imageName>

